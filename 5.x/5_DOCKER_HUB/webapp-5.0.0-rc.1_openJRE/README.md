# Asqatasun Docker image

## Quick reference

* Maintained by: [Asqatasun](https://gitlab.com/asqatasun)
* Where to get help: [Asqatasun Forum](https://forum.asqatasun.org/) or in [Asqatasun room on Element.io](https://app.element.io/#/room/#asqatasun:matrix.org)
* Where to file issues: <https://gitlab.com/asqatasun/asqatasun-docker/-/issues>

## Supported tags and respective Dockerfile links

* [`5.0.0-rc.1`](https://gitlab.com/asqatasun/asqatasun-docker/-/tree/main/5.x/5_DOCKER_HUB/webapp-5.0.0-rc.1_openJRE)

## What is Asqatasun

![Asqatasun](https://forum.asqatasun.org/uploads/default/original/1X/e16a2b9b7f5a4dc756f03630923290c695c762c9.png)

Asqatasun is an opensource website analyzer, used for web accessibility (a11y) and build with reliability in mind.

## How to use this image

### Pre-requisites

* [Docker](https://docs.docker.com/engine/install/) `19.03.0` (at least) is required
* [Docker Compose](https://docs.docker.com/compose/install/) `1.27.0` (at least) is required

### Usage

* Create a `docker-compose.yml` file with following content

```yaml
version: "3.8"
services:
    asqatasun-db:
        image: mysql:5.7
        environment:
            MYSQL_RANDOM_ROOT_PASSWORD: "yes"
            MYSQL_USER: "${DB_USER:-asqatasunDatabaseUserLogin}"
            MYSQL_PASSWORD: "${DB_PASSWORD:-asqatasunDatabaseUserP4ssword}"
            MYSQL_DATABASE: "${DB_DATABASE:-asqatasun}"
            MYSQL_TCP_PORT: "${DB_PORT:-3306}"
        ports:
            - "${DB_HOST_IP:-0.0.0.0}:${DB_HOST_PORT:-3306}:${DB_PORT:-3306}"

    asqatasun:
        image: asqatasun/asqatasun:latest
        links:
            - asqatasun-db
        depends_on:
            - asqatasun-db
        environment:
            DB_DRIVER: "${DB_DRIVER:-mysql}"
            DB_PORT: "${DB_PORT:-3306}"
            DB_HOST: "${DB_HOST:-asqatasun-db}"
            DB_USER: "${DB_USER:-asqatasunDatabaseUserLogin}"
            DB_PASSWORD: "${DB_PASSWORD:-asqatasunDatabaseUserP4ssword}"
            DB_DATABASE: "${DB_DATABASE:-asqatasun}"
        ports:
            - "${APP_HOST_IP:-0.0.0.0}:${APP_HOST_PORT:-8080}:8080"
```

* Launch with `docker-compose up`
* Point your browser to `http://127.0.0.1:8080/` and log in with:
  * login: `admin@asqatasun.org`
  * password: `myAsqaPassword`
* Tear down with `docker-compose down`


