# Launch Asqatasun `5-SNAPSHOT` Webapp

> ⚠️ Created for testing purpose only, no security has been made for production. ⚠️

## Context of use

Uses **SNAPSHOT** version of Asqatasun which is built by Gitlab CI every time source code is modified.

Each **SNAPSHOT** build can introduce new bugs, disable features or even stop working at all.

It's should be used with full knowledge of this point and only used
to test new features or check that a bug is now fixed for the next version of Asqatasun.

## Prerequisites

- [Docker](https://docs.docker.com/engine/install/) `19.03.0` (at least) is required
- [Docker Compose](https://docs.docker.com/compose/install/) `1.27.0` (at least) is required

## Ports, URL and credentials (user/password)

| Service  | Port | URL                                     | User                         | Password                        |
|----------|------|-----------------------------------------|------------------------------|---------------------------------|
| Database | 3306 | `jdbc:mysql://localhost:3306/asqatasun` | `asqatasunDatabaseUserLogin` | `asqatasunDatabaseUserP4ssword` |
| Webapp   | 8080 | `http://localhost:8080`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |

Tip:
if you copy [`.env.dist`](.env.dist) file to `.env` file,
you can change **port** numbers, **IP** adresses and **database** credentials.

## Software versions

- Asqatasun **5-SNAPSHOT**
- Geckodriver **0.30.0**
- Firefox **91.4.1esr**

## Launch Asqatasun webapp with Docker-Compose

- Build docker images
- Launch Asqatasun webapp

### Option 1: uses same database, if it already exists

```shell
# build docker image + launch Asqatasun and database (uses same database, if it already exists)
docker-compose up --build
```

### Option 2: uses a new database

```shell
# build docker image + launch Asqatasun and a new database
docker-compose rm -fsv
docker-compose up --build
```

### Option 3: uses a new database and rebuild Docker images

Recommended option for **SNAPSHOT** versions of Asqatasun that can be updated at any time.

```shell
# re-build Docker image + launch Asqatasun and a new database
docker-compose rm -fsv
docker-compose build --no-cache
docker-compose up
```

## Play with Asqatasun webapp

- In your browser, go to `http://127.0.0.1:8080/`
- Use this user and this password to log in:
  - `admin@asqatasun.org`
  - `myAsqaPassword`
