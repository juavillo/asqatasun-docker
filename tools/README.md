# Tools

[Generator](generator/) - Generate README, Dockerfile, docker-compose.yml and .env.dist files
for default or custom versions of `Asqatasun`, `Firefox` and `Geckodriver`.
