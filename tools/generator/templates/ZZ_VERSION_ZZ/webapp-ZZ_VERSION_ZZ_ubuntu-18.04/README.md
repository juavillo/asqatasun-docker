# Launch Asqatasun `ZZ-version-ZZ` Webapp

> ⚠️ Created for testing purpose only, no security has been made for production. ⚠️

## Prerequisites

- [Docker](https://docs.docker.com/engine/install/) `19.03.0` (at least) is required
- [Docker Compose](https://docs.docker.com/compose/install/) `1.27.0` (at least) is required

## Ports, URL and credentials (user/password)

| Service  | Port | URL                                     | User                         | Password                        |
|----------|------|-----------------------------------------|------------------------------|---------------------------------|
| Database | 3306 | `jdbc:mysql://localhost:3306/asqatasun` | `asqatasunDatabaseUserLogin` | `asqatasunDatabaseUserP4ssword` |
| Webapp   | 8080 | `http://localhost:8080`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |

Tip:
if you copy [`.env.dist`](.env.dist) file to `.env` file,
you can change **port** numbers, **IP** adresses and **database** credentials.

## Software versions

- Asqatasun **ZZ-version-ZZ**
- Geckodriver **ZZ-geckodriver-version-ZZ**
- Firefox **ZZ-firefox-version-ZZ**

## Launch Asqatasun webapp with Docker-Compose

- Build docker images
- Launch Asqatasun webapp

### Option 1: uses same database, if it already exists

```shell
# build docker image + launch Asqatasun and database (uses same database, if it already exists)
docker-compose up --build
```

### Option 2: uses a new database

```shell
# build docker image + launch Asqatasun and a new database
docker-compose rm -fsv
docker-compose up --build
```

## Play with Asqatasun webapp

- In your browser, go to `http://127.0.0.1:8080/`
- Use this user and this password to log in:
  - `admin@asqatasun.org`
  - `myAsqaPassword`
